﻿namespace OpeniddictRefreshToken.Models.Entities
{
    public class Attendee : IEntityBase
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public AppUser User { get; set; }

        public int ScheduleId { get; set; }
        public Schedule Schedule { get; set; }
    }
}