﻿namespace OpeniddictRefreshToken.Models.Entities
{
    public interface IEntityBase
    {
        int Id { get; set; }
    }
}