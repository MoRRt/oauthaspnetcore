﻿using OpeniddictRefreshToken.Data.Repositories;
using OpeniddictRefreshToken.Data.Repositories.Abstract;

namespace OpeniddictRefreshToken.Data
{
    public class UnitOfWork : IUnitOfWork
    {

        public ApplicationDbContext DbContext { get; }
        private IScheduleRepository scheduleRepository;
        private IAttendeeRepository attendeeRepository;
        public UnitOfWork(ApplicationDbContext dbContext)
        {
            this.DbContext = dbContext;
        }

        public IScheduleRepository ScheduleRepository
        {
            get
            {
                return scheduleRepository = scheduleRepository ?? new ScheduleRepository(DbContext);
            }
        }

        public IAttendeeRepository AttendeeRepository
        {
            get
            {
                return attendeeRepository = attendeeRepository ?? new AttendeeRepository(DbContext);
            }
        }
       
        public void Commit()
        {
            DbContext.SaveChanges();
        }
    }
}