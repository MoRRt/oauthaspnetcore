﻿using OpeniddictRefreshToken.Data.Repositories.Abstract;

namespace OpeniddictRefreshToken.Data
{
    public interface IUnitOfWork
    {
        IScheduleRepository ScheduleRepository { get; }
        IAttendeeRepository AttendeeRepository { get; }
        void Commit();
    }
}