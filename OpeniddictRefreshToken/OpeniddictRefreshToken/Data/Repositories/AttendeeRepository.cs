﻿using OpeniddictRefreshToken.Data.Repositories.Abstract;
using OpeniddictRefreshToken.Models.Entities;

namespace OpeniddictRefreshToken.Data.Repositories
{
    public class AttendeeRepository : EntityBaseRepository<Attendee>, IAttendeeRepository
    {
        public AttendeeRepository(ApplicationDbContext context)
            : base(context)
        { }
    }
}