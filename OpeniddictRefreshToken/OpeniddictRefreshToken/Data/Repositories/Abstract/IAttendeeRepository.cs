﻿using OpeniddictRefreshToken.Models.Entities;

namespace OpeniddictRefreshToken.Data.Repositories.Abstract
{
    public interface IAttendeeRepository : IEntityBaseRepository<Attendee> { }
}