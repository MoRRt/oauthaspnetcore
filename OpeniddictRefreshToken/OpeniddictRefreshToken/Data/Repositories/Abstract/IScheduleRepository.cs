﻿using OpeniddictRefreshToken.Models.Entities;

namespace OpeniddictRefreshToken.Data.Repositories.Abstract
{
    public interface IScheduleRepository : IEntityBaseRepository<Schedule> { }
}