﻿using OpeniddictRefreshToken.Data.Repositories.Abstract;
using OpeniddictRefreshToken.Models.Entities;

namespace OpeniddictRefreshToken.Data.Repositories
{
    public class ScheduleRepository : EntityBaseRepository<Schedule>, IScheduleRepository
    {
        public ScheduleRepository(ApplicationDbContext context)
            : base(context)
        { }
    }
}