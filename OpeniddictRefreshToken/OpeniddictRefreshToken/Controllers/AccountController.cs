﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AspNet.Security.OpenIdConnect.Primitives;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OpeniddictRefreshToken.Data;
using OpeniddictRefreshToken.Models.Entities;
using OpeniddictRefreshToken.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OpeniddictRefreshToken.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly UserManager<AppUser> _userManager;
        private IUnitOfWork Storage { get; set; }

        public AccountController(UserManager<AppUser> userManager,IUnitOfWork storage)
        {
            Storage = storage;
            _userManager = userManager;
        }

        
        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody] RegisterViewModel model)
        {
            
            if (ModelState.IsValid)
            {
                var user = new AppUser { UserName = model.UserName, Email = model.UserName };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    return Ok();
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed.
            return BadRequest(ModelState);
        }

        [HttpGet("me", Name = "CurrentUser")]
        public async Task<IActionResult> GetCurrentUser()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null) return Ok("No user / not logged in");// if Authorize is not applied

            var claims = User.Claims.Select(claim => new { claim.Type, claim.Value }).ToArray();
            return Json(claims);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] UserViewModel user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            AppUser _userDb = await _userManager.GetUserAsync(User);

            if (_userDb == null)
            {
                return NotFound();
            }
            else
            {
                _userDb.Name = user.Name;
                _userDb.Profession = user.Profession;
                _userDb.Avatar = user.Avatar;
                Storage.Commit();
            }

            user = Mapper.Map<AppUser, UserViewModel>(_userDb);

            return new JsonResult(user);
        }
    }
}
