﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OpeniddictRefreshToken.Data;
using OpeniddictRefreshToken.Models.Entities;
using OpeniddictRefreshToken.Utils;
using OpeniddictRefreshToken.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OpeniddictRefreshToken.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly UserManager<AppUser> _userManager;
        private IUnitOfWork Storage { get; set; }
        int page = 1;
        int pageSize = 10;

        public UsersController(UserManager<AppUser> userManager, IUnitOfWork storage)
        {
            _userManager = userManager;
            Storage = storage;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var pagination = Request.Headers["Pagination"];

            if (!string.IsNullOrEmpty(pagination))
            {
                string[] vals = pagination.ToString().Split(',');
                int.TryParse(vals[0], out page);
                int.TryParse(vals[1], out pageSize);
            }

            int currentPage = page;
            int currentPageSize = pageSize;
            var totalUsers = _userManager.Users.Count();
            var totalPages = (int)Math.Ceiling((double)totalUsers / pageSize);

            IEnumerable<AppUser> _users = _userManager.Users
                .Include(u => u.SchedulesCreated)
                .OrderBy(u => u.Id)
                .Skip((currentPage - 1) * currentPageSize)
                .Take(currentPageSize)
                .ToList();
                

            IEnumerable<UserViewModel> _usersVM = Mapper.Map<IEnumerable<AppUser>, IEnumerable<UserViewModel>>(_users);

            Response.AddPagination(page, pageSize, totalUsers, totalPages);

            return new OkObjectResult(_usersVM);
        }
    }
}
