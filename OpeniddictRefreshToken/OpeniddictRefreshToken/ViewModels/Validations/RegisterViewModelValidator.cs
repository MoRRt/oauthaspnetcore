﻿using FluentValidation;

namespace OpeniddictRefreshToken.ViewModels.Validations
{
    public class RegisterViewModelValidator : AbstractValidator<RegisterViewModel>
    {
        public RegisterViewModelValidator()
        {
            RuleFor(user => user.UserName).NotEmpty().WithMessage("Username cannot be empty");
            RuleFor(user => user.Password).NotEmpty().WithMessage("Password cannot be empty");
        }
    }
}