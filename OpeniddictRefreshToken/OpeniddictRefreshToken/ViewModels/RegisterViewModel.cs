﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using OpeniddictRefreshToken.ViewModels.Validations;

namespace OpeniddictRefreshToken.ViewModels
{
    public class RegisterViewModel : IValidatableObject
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validator = new RegisterViewModelValidator();
            var result = validator.Validate(this);
            return result.Errors.Select(item => new ValidationResult(item.ErrorMessage, new[] { item.PropertyName }));
        }
    }
}