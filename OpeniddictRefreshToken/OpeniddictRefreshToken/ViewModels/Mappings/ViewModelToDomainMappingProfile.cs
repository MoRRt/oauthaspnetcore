﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using OpeniddictRefreshToken.Models.Entities;

namespace OpeniddictRefreshToken.ViewModels.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<ScheduleViewModel, Schedule>()
                .ForMember(s => s.Creator, map => map.NullSubstitute(null))
                .ForMember(s => s.Attendees, map => map.UseValue(new List<Attendee>()));

            CreateMap<RegisterViewModel, AppUser>();
        }
    }

    public class Test
    {
        public void TestMethod(int[] array)
        {
           
            ICollection<UserViewModel> a = new List<UserViewModel>();
            a.AsParallel();
        }
    }
}